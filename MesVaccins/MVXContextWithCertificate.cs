﻿using System;
using MesVaccins.Requests;
using RestSharp;
using System.Security.Cryptography.X509Certificates;

namespace MesVaccins
{
	public class MVXContextWithCertificate : MVXContext
	{
		private readonly string _certificateFileName;
		private string _certificatePassword;
		private string _currentSession;

		public MVXContextWithCertificate(string applicationToken, string applicationSecret, string certificateFileName, string certificatePassword) : base(applicationToken, applicationSecret)
		{
			_certificateFileName = certificateFileName;
			_certificatePassword = certificatePassword;
		}

		public override void ConnectHealthProfessional(IHealthProfessional healthProfessional)
		{
			var loginRequest = new SessionLoginRequest(healthProfessional.MVXOrganisationId, healthProfessional.MVXLogin);
			var response = loginRequest.Execute(this);
			if (response.Status == 0)
			{
				_currentSession = response.Sessionids;
				ConnectedHealthProfessional = healthProfessional;
			}
			else
			{
				throw new Exception("IDS error: " + response.GetStatusDescription());
			}
		}

		protected override IRestClient PrepareClient(RestRequest request)
		{
			CheckSession();
			request.AddCookie("sessionids", _currentSession);
			return CreateClient();
		}

		private void CheckSession()
		{
			var heartbeat = new SessionIsLoggedRequest(_currentSession);
			if (!heartbeat.Execute(this))
			{
				ConnectHealthProfessional(ConnectedHealthProfessional);
			}
		}

		internal override RestClient CreateClient(string url = null)
		{
			var client = base.CreateClient(url);
			client.ClientCertificates = GetCertificates();
			return client;
		}

		private X509CertificateCollection GetCertificates()
		{
			var collection = new X509CertificateCollection();
			collection.Add(new X509Certificate2(_certificateFileName, _certificatePassword));
			return collection;
		}

		protected override Uri GetApplicationUri()
		{
			switch(Environment)
			{
			case EnvironmentName.Staging:
				return new Uri("https://test-pro-secure.mesvaccins.net/mvx_api/v1");
			default:
				return base.GetApplicationUri();
			}
		}
	}
}

