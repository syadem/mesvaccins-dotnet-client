﻿namespace MesVaccins
{
    public interface IHealthProfessional
    {
        string MVXLogin { get; }
		string MVXPassword { get; }
		int MVXOrganisationId { get; }
    }
}
