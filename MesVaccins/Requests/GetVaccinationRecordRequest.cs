﻿using System;
using System.Collections.Generic;
using System.Net;
using MesVaccins.Models;
using RestSharp;

namespace MesVaccins.Requests
{
	public class GetVaccinationRecordRequest : IRequest<ImmunizationRecord>
    {
		private string _uuid;

		public GetVaccinationRecordRequest(string uuid)
		{
			_uuid = uuid;
		}

		public ImmunizationRecord Execute(MVXContext context)
        {
            var request = new RestRequest("records/" + _uuid + ".json", Method.GET);
			var response = context.SendRequest<ImmunizationRecord>(request);
			if (response.StatusCode != HttpStatusCode.OK)
			{
				throw new Exception ("Http error code " + response.StatusCode + ": " + response.ErrorMessage + "\n" + response.Content, response.ErrorException);
			}
            return response.Data;
        }
    }
}
