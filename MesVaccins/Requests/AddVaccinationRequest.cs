﻿using System;
using MesVaccins.Models;
using RestSharp;
using System.Net;

namespace MesVaccins.Requests
{
	public class AddVaccinationRequest : IRequest<Boolean>
	{
		private ImmunizationRecord _record;
		private Vaccination _vaccination;

		public AddVaccinationRequest(ImmunizationRecord record, Vaccination vaccination)
		{
			_record = record;
			_vaccination = vaccination;
		}

		public Boolean Execute(MVXContext context)
		{
			var request = new RestRequest("records/" + _record.Uuid + "/vaccinations.json", Method.POST);
			request.RequestFormat = DataFormat.Json;
			request.AddBody(new {
				vaccine = new {id = _vaccination.Vaccine.Id.ToString()},
				date = _vaccination.Date,
				batch = _vaccination.Batch,
				booster = _vaccination.Booster,
				validation = new {administered = _vaccination.Validation.Administered}
			});
			var response = context.SendRequest(request);
			if (response.StatusCode != HttpStatusCode.Created)
			{
				throw new Exception ("Http error code " + response.StatusCode + ": " + response.ErrorMessage + "\n" + response.Content);
			}
			return true;
		}
	}
}

