﻿using System;
using RestSharp;
using System.Net;
using System.Text;

namespace MesVaccins.Requests
{
	public class GetCertificateRequest : IRequest<GetCertificateRequest.Response>
	{
		private readonly string _otp;

		public GetCertificateRequest(string otp)
		{
			_otp = otp;
		}

		public Response Execute(MVXContext context)
		{
			var client = new RestClient(GetIDSUrl(MVXContext.Environment));
			var request = new RestRequest("restgetcertificate.php", Method.POST);
			request.RequestFormat = DataFormat.Json;
			request.AddBody( new { identifier = "none", otp = _otp, type = "p12" });
			var response = client.Execute<Response>(request);
			if (response.StatusCode != HttpStatusCode.OK)
			{
				throw new Exception ("Http error code " + response.StatusCode + ": " + response.ErrorMessage + "\n" + response.Content, response.ErrorException);
			}
			return response.Data;
		}

		private string GetIDSUrl(MesVaccins.MVXContext.EnvironmentName environment)
		{
			switch (environment)
			{
			case MVXContext.EnvironmentName.Staging:
				return "https://certtest.idshost.fr";
			case MVXContext.EnvironmentName.Production:
				return "https://cert.idshost.fr";
			}
			return null;
		}

		public class Response
		{
			public int Status { get; set; }
			public string Certbase64encoded { get; set; }

			public byte[] Cert
			{ 
				get { return Convert.FromBase64String(Certbase64encoded); }
			}

			public string GetStatusDescription()
			{
				switch (Status)
				{
				case 0:
					return "OK";
				case 1:
					return "Requête mal formée";
				case 2:
					return "Type incorrect (pem/p12)";
				case 3:
					return "Identifiant ou OTP incorrect";
				case 4:
					return "Erreur serveur, contactez nous à developers@mesvaccins.net";
				}
				return "";
			}
		}
	}
}
