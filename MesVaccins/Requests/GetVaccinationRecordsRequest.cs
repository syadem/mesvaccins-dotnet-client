﻿using System;
using System.Collections.Generic;
using System.Net;
using MesVaccins.Models;
using RestSharp;

namespace MesVaccins.Requests
{
	public class GetVaccinationRecordsRequest : IRequest<IEnumerable<ImmunizationRecord>>
    {
		public IEnumerable<ImmunizationRecord> Execute(MVXContext context)
        {
            var request = new RestRequest("records.json", Method.GET);
			var response = context.SendRequest<List<ImmunizationRecord>>(request);
			if (response.StatusCode != HttpStatusCode.OK)
			{
				throw new Exception ("Http error code " + response.StatusCode + ": " + response.ErrorMessage + "\n" + response.Content, response.ErrorException);
			}
            return response.Data;
        }
    }
}
