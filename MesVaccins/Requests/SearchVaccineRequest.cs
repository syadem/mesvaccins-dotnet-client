﻿using System;
using MesVaccins.Models;
using System.Collections.Generic;
using RestSharp;
using System.Net;


namespace MesVaccins.Requests
{
	public class SearchVaccineRequest : IRequest<IEnumerable<Vaccine>>
	{
		private string _name;

		public SearchVaccineRequest(string name)
		{
			_name = name;
		}

		public IEnumerable<Vaccine> Execute(MVXContext context)
		{
			var request = new RestRequest("vaccines/search.json", Method.GET);
			request.AddParameter("query", _name);
			var response = context.SendRequest<List<Vaccine>>(request);
			if (response.StatusCode != HttpStatusCode.OK)
			{
				throw new Exception ("Http error code " + response.StatusCode + ": " + response.ErrorMessage + "\n" + response.Content, response.ErrorException);
			}
			return response.Data;
		}
	}
}
