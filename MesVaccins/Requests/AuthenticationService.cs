﻿using System;
using RestSharp;

namespace MesVaccins
{
	public abstract class AuthenticationService
	{
		protected RestClient GetClient(MVXContext context)
		{
			return context.CreateClient(GetServiceUrl(MVXContext.Environment));
		}

		private string GetServiceUrl(MesVaccins.MVXContext.EnvironmentName environment)
		{
			switch (environment)
			{
			case MVXContext.EnvironmentName.Staging:
				return "https://test-pro-secure.mesvaccins.net/authenticationids";
			case MVXContext.EnvironmentName.Production:
				return "https://pro-secure.mesvaccins.net/authenticationids";
			}
			return null;
		}
	}
}

