﻿using System;

namespace MesVaccins.Requests
{
	public interface IRequest<T>
	{
		T Execute(MVXContext context);
	}
}

