﻿using System;
using System.Net;
using System.Text;
using RestSharp;

namespace MesVaccins.Requests
{
	public class SessionLoginRequest : AuthenticationService, IRequest<SessionLoginRequest.Response>
	{
		private readonly string _login;
		private readonly string _password;
		private readonly int? _organisationId;

		public SessionLoginRequest(int organisationId, string login) : this(login, null)
		{
			_organisationId = organisationId;
		}

		public SessionLoginRequest(string login, string password)
		{
			_login = login;
			_password = password;
		}

		public Response Execute(MVXContext context)
		{
			var password = _password ?? ComputeHashedPassword(context.ApplicationToken);

			var client = GetClient(context);
			var request = new RestRequest("restloginservice.php", Method.POST);
			request.RequestFormat = DataFormat.Json;
			request.AddBody( new { authentifier = _login, password = password });
			var response = client.Execute<Response>(request);
			if (response.StatusCode != HttpStatusCode.OK)
			{
				throw new Exception ("Http error code " + response.StatusCode + ": " + response.ErrorMessage ?? "", response.ErrorException);
			}
			return response.Data;
		}

		private string ComputeHashedPassword(string applicationToken)
		{
			if (_organisationId == null)
			{
				throw new Exception("Configuration error: organisation id must be set");
			}
			return BCrypt.Net.BCrypt.HashPassword(_login + _organisationId.ToString(), "$2a$10$" + applicationToken);
		}

		public class Response
		{
			public int Status { get; set; }
			public string Sessionids { get; set; }

			public string GetStatusDescription()
			{
				switch (Status)
				{
				case 0:
					return "OK";
				case 1:
					return "Requête mal formée";
				case 2:
					return "Certificat non accepté";
				case 3:
					return "Identifiant manquant (mot de passe et/ou authentifiant)";
				case 4:
					return "Certificat en blacklist";
				case 5:
					return "Identifiants invalides";
				}
				return "";
			}
		}
	}
}
