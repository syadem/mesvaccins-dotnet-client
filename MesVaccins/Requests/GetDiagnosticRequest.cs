﻿using System;
using RestSharp;
using System.Net;
using System.Text;
using MesVaccins.Models;

namespace MesVaccins.Requests
{
	public class GetDiagnosticRequest : IRequest<Assessment>
	{
		private readonly ImmunizationRecord _record;

		public GetDiagnosticRequest(ImmunizationRecord record)
		{
			_record = record;
		}

		public Assessment Execute(MVXContext context)
		{
			var request = new RestRequest("records/" + _record.Uuid + "/immunisation_assessment.json", Method.GET);
			var response = context.SendRequest<Assessment>(request);
			if (response.StatusCode != HttpStatusCode.OK)
			{
				throw new Exception ("Http error code " + response.StatusCode + ": " + response.ErrorMessage + "\n" + response.Content, response.ErrorException);
			}
			return response.Data;
		}
	}
}
