﻿using System;
using RestSharp;
using System.Net;
using System.Text;

namespace MesVaccins.Requests
{
	public class SessionIsLoggedRequest : AuthenticationService, IRequest<bool>
	{
		private readonly string _session;

		public SessionIsLoggedRequest(string session)
		{
			_session = session;
		}

		public bool Execute(MVXContext context)
		{
			var client = GetClient(context);
			var request = new RestRequest("restisloggedservice.php", Method.POST);
			request.RequestFormat = DataFormat.Json;
			request.AddBody( new { sessionids = _session });
			var response = client.Execute<Response>(request);
			if (response.StatusCode != HttpStatusCode.OK)
			{
				throw new Exception("Http error code " + response.StatusCode + ": " + response.ErrorMessage + "\n" + response.Content, response.ErrorException);
			}
			return response.Data.Status == 0;
		}

		public class Response
		{
			public int Status { get; set; }
		}
	}
}
