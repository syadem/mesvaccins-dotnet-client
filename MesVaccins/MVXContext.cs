﻿using System;
using RestSharp;
using System.Net;
using MesVaccins.Requests;
using System.Security.Cryptography.X509Certificates;
using System.IO;

namespace MesVaccins
{
	public abstract class MVXContext
    {
        public enum EnvironmentName
        {
            Staging,
            Production
        }

		protected IHealthProfessional ConnectedHealthProfessional
		{
			get;
			set;
		}

		private readonly string _applicationSecret;

		internal string ApplicationToken
		{ 
			get;
			private set;
		}

        private static EnvironmentName _environment = EnvironmentName.Staging;
        public static EnvironmentName Environment
        {
            get { return _environment; }
            set { _environment = value; }
        }

		public MVXContext(string applicationToken, string applicationSecret)
		{
			ApplicationToken = applicationToken;
			_applicationSecret = applicationSecret;
		}

		public abstract void ConnectHealthProfessional(IHealthProfessional healthProfessional);

		internal IRestResponse<T> SendRequest<T>(RestRequest request) where T: new()
        {
			return PrepareClient(request).Execute<T>(request);
        }

		internal IRestResponse SendRequest(RestRequest request)
		{
			return PrepareClient(request).Execute(request);
		}

		protected abstract IRestClient PrepareClient(RestRequest request);

		internal virtual RestClient CreateClient(string url = null)
        {
			url = url ?? GetApplicationUri().AbsoluteUri;
            var client = new RestClient(url)
            {
				Authenticator = new HttpBasicAuthenticator(ApplicationToken, _applicationSecret),
            };
            return client;
        }
			
        protected virtual Uri GetApplicationUri()
        {
            switch(Environment)
            {
            case EnvironmentName.Production:
            	return new Uri("https://pro-secure.mesvaccins.net/mvx_api/v1");
            }
            return null;
        }
    }
}
