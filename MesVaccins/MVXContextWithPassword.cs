﻿using System;
using RestSharp;

namespace MesVaccins
{
	public class MVXContextWithPassword : MVXContext
	{
		public MVXContextWithPassword(string applicationToken, string applicationSecret) : base(applicationToken, applicationSecret)
		{
		}

		public override void ConnectHealthProfessional(IHealthProfessional healthProfessional)
		{
			ConnectedHealthProfessional = healthProfessional;
		}

		protected override IRestClient PrepareClient(RestRequest request)
		{
			request.AddHeader("MVX-Health-Professional-Login", ConnectedHealthProfessional.MVXLogin);
			request.AddHeader("MVX-Health-Professional-Password", ConnectedHealthProfessional.MVXPassword);
			return CreateClient();
		}

		protected override Uri GetApplicationUri()
		{
			switch(Environment)
			{
			case EnvironmentName.Staging:
				return new Uri("http://test-pro.mesvaccins.net/mvx_api/v1");
			default:
				return base.GetApplicationUri();
			}
		}
	}
}

