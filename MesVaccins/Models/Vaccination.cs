﻿using System;

namespace MesVaccins.Models
{
	public class Vaccination
	{
		public int Id { get; set; }
		public string Date { get; set; }
		public string Batch { get; set; }
		public bool Booster { get; set; }
		public Vaccine Vaccine { get; set; }
		public Validation Validation { get; set; }
	}
}

