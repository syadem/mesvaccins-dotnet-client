﻿using System;
using System.Collections.Generic;

namespace MesVaccins.Models
{
	[Serializable()]
	public class Assessment
	{
		public string schedule_status { get; set; }
		public List<DiseaseAssessment> assessments { get; set; }
	}
}