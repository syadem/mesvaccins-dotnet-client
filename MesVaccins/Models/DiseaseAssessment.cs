﻿using System;

namespace MesVaccins.Models
{
	[Serializable()]
	public class DiseaseAssessment
	{
		public string disease { get; set; }
		public string conditions { get; set; }
		public int dose_count { get; set; }
		public string message { get; set; }
		public string schedule_status { get; set; }
		public string next_vaccination_date { get; set; }
		public string recommendation { get; set; }
	}
}