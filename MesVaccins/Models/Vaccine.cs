﻿using System;

namespace MesVaccins.Models
{
	public class Vaccine
	{
		public string Id { get; set; }
		public string Name { get; set; }
	}
}
