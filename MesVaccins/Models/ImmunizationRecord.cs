﻿using System.Collections.Generic;

namespace MesVaccins.Models
{
    public class ImmunizationRecord
    {
        public string Uuid { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
		public List<Vaccination> Vaccinations { get; set; }

		public string FullName
		{
			get { return Firstname + " " + Lastname; }
		}
    }
}
