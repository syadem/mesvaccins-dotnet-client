﻿using System;
using VacciTest.Views;
using MesVaccins.Models;

namespace VacciTest.Views
{
	public class ShowDiagnosticView : View
	{
		public Action<ImmunizationRecord> QuitScreen;

		private ImmunizationRecord _record;
		private Assessment _assessments;

		public ShowDiagnosticView(ImmunizationRecord record, Assessment assessments)
		{
			_record = record;
			_assessments = assessments;
		}

		public override void Display()
		{
			Console.WriteLine(_assessments.schedule_status.ToString());
			Console.ReadLine();
			QuitScreen(_record);
		}
	}
}
