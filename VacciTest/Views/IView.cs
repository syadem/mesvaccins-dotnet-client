﻿using System;

namespace VacciTest.Views
{
	public interface IView
	{
		void Display();
	}
}

