﻿using System;
using MesVaccins.Models;
using VacciTest.Views;

namespace VacciTest
{
	public class ShowAddVaccinationView : View
	{
		public Func<string, Vaccine> CheckVaccine;
		public Action<ImmunizationRecord, Vaccination> AddVaccination;

		private ImmunizationRecord _record;

		public ShowAddVaccinationView(ImmunizationRecord record)
		{
			_record = record;
		}

		public override void Display()
		{
			Console.WriteLine("Ajout d'une vaccination");
			Console.WriteLine();
			Console.Write("Date d'injection : ");
			var date = Console.ReadLine();

			Vaccine vaccine = null;
			while (vaccine == null)
			{
				Console.Write("Nom du vaccin : ");
				var vaccineName = Console.ReadLine();
				vaccine = CheckVaccine(vaccineName);
				if (vaccine == null)
				{
					Console.WriteLine("Vaccin inconnu !");
				}
			}

			Console.Write("Vous avez administré ce vaccin (O/N) : ");
			var administered = Console.ReadLine();
			Console.Write("Cette injection est un rappel (O/N) : ");
			var booster = Console.ReadLine();
			Console.Write("N° de lot : ");
			var batch = Console.ReadLine();

			var vaccination = new Vaccination {
				Vaccine = vaccine,
				Date = DateTime.Parse(date).ToString("yyyy-MM-dd"),
				Booster = booster == "O",
				Batch = batch,
				Validation = new Validation { Administered = administered == "O" }
			};
			AddVaccination(_record, vaccination);
		}
	}
}
