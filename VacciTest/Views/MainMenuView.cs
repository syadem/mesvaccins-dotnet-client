﻿using System;
using System.Collections.Generic;
using MesVaccins.Models;
using System.Linq;

namespace VacciTest.Views
{
	public class MainMenuView : View
	{
		public Action<ImmunizationRecord> ShowImmunizationRecord;

		private string _login;
		private IList<ImmunizationRecord> _immunizationRecords;

		public MainMenuView(string login, IList<ImmunizationRecord> immunizationRecords)
		{
			_login = login;
			_immunizationRecords = immunizationRecords;
		}

		public override void Display()
		{
			base.Display();
			Console.Out.WriteLine("Bienvenue {0} !", _login);
			Console.Out.WriteLine();
			Console.Out.WriteLine("Vos carnets :");
			for (var i = 0; i < _immunizationRecords.Count(); i++)
			{
				var immunizationRecord = _immunizationRecords[i];
				Console.Out.WriteLine("{0} - {1} (uuid={2})", i + 1, immunizationRecord.Firstname, immunizationRecord.Uuid);
			}
			Console.Out.Write("Sélectionner un carnet : ");
			var choice = Console.ReadLine();
			int recordIndex;
			if (int.TryParse(choice, out recordIndex) && recordIndex > 0 && recordIndex <= _immunizationRecords.Count())
			{
				ShowImmunizationRecord(_immunizationRecords[recordIndex - 1]);
			}
			else
			{
				Console.Out.WriteLine("Cet identifiant ne correspond pas à un carnet existant.");
			}
		}
	}
}

