﻿using System;

namespace VacciTest.Views
{
	public abstract class View : IView
	{
		public virtual void Display()
		{
			Console.Clear();
			Console.Out.WriteLine("== VacciTest ==");
			Console.Out.WriteLine();
			Console.Out.WriteLine();
		}
	}
}

