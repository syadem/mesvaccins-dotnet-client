﻿using System;

namespace VacciTest.Views
{
	public class LoginView : View
	{
		public event Action<string, string> Login;

		public override void Display()
		{
			base.Display();
			bool validated = false;
			while (!validated)
			{
				try
				{
					Console.Out.Write("Login : ");
					var login = Console.ReadLine();
					Console.Out.Write("Mot de passe : ");
					var password = Console.ReadLine();
					Login(login, password);
					validated = true;
				}
				catch(Exception e)
				{
					Console.Out.WriteLine(e.Message);
				}
			}
		}
	}
}

