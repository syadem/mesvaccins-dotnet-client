﻿using System;
using VacciTest.Views;
using MesVaccins.Models;

namespace VacciTest.Views
{
	public class ShowImmunizationRecordView : View
	{
		public Action<ImmunizationRecord> ShowAddVaccination;
		public Action<ImmunizationRecord> ShowDiagnostic;

		private ImmunizationRecord _record; 

		public ShowImmunizationRecordView(ImmunizationRecord record)
		{
			_record = record;
		}

		public override void Display()
		{
			Console.Out.WriteLine("Vaccinations de {0} :", _record.FullName);
			for (var i = 0; i < _record.Vaccinations.Count; i++)
			{
				var v = _record.Vaccinations[i];
				var booster = v.Booster ? "(rappel)" : "";
				Console.Out.WriteLine("{0} : {1} {2}", v.Date, v.Vaccine.Name, booster);
			}
			Console.WriteLine("A - Ajouter une vaccination");
			Console.WriteLine("D - Voir le diagnostic");
			Console.WriteLine();
			Console.Out.Write("Choix : ");
			var choice = Console.ReadLine();
			if (choice.ToUpper() == "A")
			{
				ShowAddVaccination(_record);
			}
			else if (choice.ToUpper() == "D")
			{
				ShowDiagnostic(_record);
			}
			else
			{
				Console.WriteLine("Ce choix n'est pas disponible");
			}
		}
	}
}
