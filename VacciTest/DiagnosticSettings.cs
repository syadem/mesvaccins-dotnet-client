﻿using System;
using System.IO;
using System.Collections.Generic;

namespace VacciTest
{
	public class DiagnosticSettings : Settings
	{
		public static void Enable()
		{
			_instance = new DiagnosticSettings();
		}

		protected DiagnosticSettings()
		{
		}

		private string _otp;
		public override string OTP
		{
			get
			{
				return _otp;
			}
			set
			{
				_otp = value;
			}
		}

		public override string CertificateFileName
		{
			get { return "./certificate.diagnostic.p12"; }
		}

		private int _organisationId;
		public override int OrganisationId
		{
			get
			{
				return _organisationId;
			}
			set
			{
				_organisationId = value;
			}
		}

		private IDictionary<string, string> _licence;
		public override IDictionary<string, string> Licence
		{
			get
			{
				return _licence;
			}
			set
			{
				_licence = value;
			}
		}
	}
}

