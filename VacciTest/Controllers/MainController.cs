﻿using System;
using MesVaccins;
using VacciTest.Models;
using VacciTest.Views;
using MesVaccins.Requests;
using System.Linq;
using MesVaccins.Models;
using System.Collections.Generic;

namespace VacciTest.Controllers
{
	public class MainController
	{
		enum State {
			Login,
			MainMenu,
			ShowImmunizationRecord,
			ShowAddVaccination,
			ShowDiagnostic
		}

		private MVXContext _context;
		private HealthProfessional _loggedProfessional;
		private State _currentState;
		private State? _nextState;
		private object _stateAttachedData;
		private IView _currentView;

		public MainController(MVXContext context)
		{
			_context = context;

			ChangeState(State.Login);
		}

		private void ChangeState(State state)
		{
			if (_currentView != null)
			{
				ReleaseCurrentView();
			}
			_currentState = state;
			_currentView = CreateView(_currentState);
			_currentView.Display();
			if (_nextState.HasValue)
			{
				ChangeState(_nextState.Value);
				_nextState = null;
			}
		}

		private IView CreateView(State state)
		{
			switch (state)
			{
			case State.Login:
				var loginView = new LoginView();
				loginView.Login += OnLogin;
				return loginView;
			case State.MainMenu:
				var getVaccinationRecordsRequest = new GetVaccinationRecordsRequest();
				var immunizationRecords = getVaccinationRecordsRequest.Execute(_context).ToList();
				var mainMenuView = new MainMenuView(_loggedProfessional.Login, immunizationRecords);
				mainMenuView.ShowImmunizationRecord += OnShowImmunizationRecord;
				return mainMenuView;
			case State.ShowImmunizationRecord:
				var getVaccinationRecordRequest = new GetVaccinationRecordRequest((_stateAttachedData as ImmunizationRecord).Uuid);
				var immunizationRecord = getVaccinationRecordRequest.Execute(_context);
				var showImmunizationRecord = new ShowImmunizationRecordView(immunizationRecord);
				showImmunizationRecord.ShowAddVaccination += OnShowAddVaccination;
				showImmunizationRecord.ShowDiagnostic += OnShowDiagnostic;
				return showImmunizationRecord;
			case State.ShowAddVaccination:
				var showAddVaccination = new ShowAddVaccinationView(_stateAttachedData as ImmunizationRecord);
				showAddVaccination.AddVaccination += OnAddVaccination;
				showAddVaccination.CheckVaccine += OnCheckVaccine;
				return showAddVaccination;
			case State.ShowDiagnostic:
				var getDiagnosticRequest = new GetDiagnosticRequest(_stateAttachedData as ImmunizationRecord);
				var diagnostic = getDiagnosticRequest.Execute(_context);
				var showDiagnostic = new ShowDiagnosticView(_stateAttachedData as ImmunizationRecord, diagnostic);
				showDiagnostic.QuitScreen += OnShowImmunizationRecord;
				return showDiagnostic;
			}
			return null;
		}

		private void ReleaseCurrentView()
		{
			switch (_currentState)
			{
			case State.Login:
				(_currentView as LoginView).Login -= OnLogin;
				break;
			case State.MainMenu:
				(_currentView as MainMenuView).ShowImmunizationRecord -= OnShowImmunizationRecord;
				break;
			case State.ShowImmunizationRecord:
				(_currentView as ShowImmunizationRecordView).ShowAddVaccination -= OnShowAddVaccination;
				(_currentView as ShowImmunizationRecordView).ShowDiagnostic -= OnShowDiagnostic;
				break;
			case State.ShowAddVaccination:
				(_currentView as ShowAddVaccinationView).AddVaccination -= OnAddVaccination;
				(_currentView as ShowAddVaccinationView).CheckVaccine -= OnCheckVaccine;
				break;
			case State.ShowDiagnostic:
				(_currentView as ShowDiagnosticView).QuitScreen -= OnShowImmunizationRecord;
				break;
			}
		}

		public void OnLogin(string login, string password)
		{
			_loggedProfessional = HealthProfessional.FindWithCredentials(login, password);
			_context.ConnectHealthProfessional(_loggedProfessional);
			_nextState = State.MainMenu;
		}

		public void OnShowImmunizationRecord(ImmunizationRecord immunizationRecord)
		{
			_nextState = State.ShowImmunizationRecord;
			_stateAttachedData = immunizationRecord;
		}

		public void OnShowAddVaccination(ImmunizationRecord immunizationRecord)
		{
			_nextState = State.ShowAddVaccination;
			_stateAttachedData = immunizationRecord;
		}

		public void OnShowDiagnostic(ImmunizationRecord immunizationRecord)
		{
			_nextState = State.ShowDiagnostic;
			_stateAttachedData = immunizationRecord;
		}

		public Vaccine OnCheckVaccine(string name)
		{
			var request = new SearchVaccineRequest(name);
			return request.Execute(_context).FirstOrDefault();
		}

		public void OnAddVaccination(ImmunizationRecord immunizationRecord, Vaccination vaccination)
		{
			var request = new AddVaccinationRequest(immunizationRecord, vaccination);
			request.Execute(_context);
			_nextState = State.ShowImmunizationRecord;
			_stateAttachedData = immunizationRecord;
		}
	}
}