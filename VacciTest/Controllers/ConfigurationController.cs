﻿using System;
using MesVaccins.Requests;
using System.IO;
using System.Collections.Generic;

namespace VacciTest.Controllers
{
	public class ConfigurationController
	{
		public ConfigurationController(string certificateFileName, bool askForLicence)
		{
			Console.Out.WriteLine("== Configuration de VacciTest ==");
			if (askForLicence)
			{
				GetLicence();
			}
			GetOrganisationId();
			InstallCertificate(certificateFileName);
		}

		private void GetLicence()
		{
			var done = false;
			while (!done)
			{
				try
				{
					Console.Out.Write("Application Token UID : ");
					var token = Console.ReadLine();
					Console.Out.Write("Application Secret : ");
					var secret = Console.ReadLine();
					Settings.Instance.Licence = new Dictionary<string, string> {
						{ "applicationToken", token },
						{ "applicationSecret", secret }
					};
					done = true;
				}
				catch(Exception e)
				{
					Console.Out.WriteLine(e.Message);
				}
			}
		}

		private void GetOrganisationId()
		{
			var done = false;
			while (!done)
			{
				try
				{
					Console.Out.Write("Id de l'organisation : ");
					var organisationId = Console.ReadLine();
					Settings.Instance.OrganisationId = int.Parse(organisationId);
					done = true;
				}
				catch(Exception e)
				{
					Console.Out.WriteLine(e.Message);
				}
			}
		}

		private void InstallCertificate(string certificateFileName)
		{
			var done = false;
			while (!done)
			{
				try
				{
					Console.Out.Write("Code de retrait : ");
					var otp = Console.ReadLine();
					var request = new GetCertificateRequest(otp);
					var response = request.Execute(null);
					if (response.Status == 0)
					{
						using (BinaryWriter writer = new BinaryWriter(File.Open(certificateFileName, FileMode.Create)))
						{
							writer.Write(response.Cert);
						}
						Settings.Instance.OTP = otp;
						done = true;
					}
					else
					{
						throw new Exception("IDS error: " + response.GetStatusDescription());
					}
				}
				catch(Exception e)
				{
					Console.Out.WriteLine(e.Message);
				}
			}
		}
	}
}

