﻿using MesVaccins;
using System;

namespace VacciTest.Models
{
    class HealthProfessional : IHealthProfessional
    {
		public string Login { get; set; }

        public string MVXLogin { get; set; }
		public string MVXPassword { get; set; }
		public int MVXOrganisationId 
		{ 
			get { return Settings.Instance.OrganisationId; }
		}

		private HealthProfessional(string login)
		{
			Login = login;
			MVXLogin = "pierre.borsan@gmail.com";
			MVXPassword = "4mzZipL9RzGP";
		}

		public static HealthProfessional FindWithCredentials(string login, string password)
		{
			if (!string.IsNullOrWhiteSpace(password))
			{
				return new HealthProfessional(login);
			}
			else
			{
				throw new Exception("Application error: wrong login or password");
			}
		}
    }
}
