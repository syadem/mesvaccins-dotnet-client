﻿using System;
using System.IO;
using System.Collections.Generic;

namespace VacciTest
{
	public class Settings
	{
		private const string LicenceFileName = "./licence.txt";

		protected static Settings _instance;
		public static Settings Instance
		{
			get
			{
				if (_instance == null)
				{
					_instance = new Settings();
				}
				return _instance;
			}
		}

		protected Settings()
		{
		}

		public virtual string OTP
		{
			get
			{
				return File.ReadAllText("./otp.txt");
			}
			set
			{
				File.WriteAllText("./otp.txt", value);
			}
		}

		public virtual string CertificateFileName
		{
			get { return "./certificate.p12"; }
		}

		public virtual int OrganisationId
		{
			get
			{
				return int.Parse(File.ReadAllText("./organisation.txt"));
			}
			set
			{
				File.WriteAllText("./organisation.txt", value.ToString());
			}
		}

		public virtual IDictionary<string, string> Licence
		{
			get
			{
				var licence = File.ReadAllLines(LicenceFileName);
				return new Dictionary<string, string> {
					{ "applicationToken", licence[0] },
					{ "applicationSecret", licence[1] }
				};
			}
			set
			{
			}
		}
	}
}

