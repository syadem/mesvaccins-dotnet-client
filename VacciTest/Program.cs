﻿using System;
using System.Linq;
using MesVaccins.Requests;
using MesVaccins;
using System.Configuration;
using VacciTest.Controllers;
using System.IO;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Net;
using System.Collections.Generic;

namespace VacciTest
{
	class Program
	{
		static void Main(string[] args)
		{
			ServicePointManager.ServerCertificateValidationCallback = Validator;

			try
			{
				if (args.Any(a => a.Equals("-diagnostic")))
				{
					DiagnosticSettings.Enable();
					ConfigureApplication(true);
				}
				else if (!File.Exists(Settings.Instance.CertificateFileName))
				{
					ConfigureApplication();
				}

				InitializeApplication();
			}
			catch(Exception e)
			{
				Console.Out.WriteLine("Exception: {0}\n{1}", e.Message, e.StackTrace);
			}
		}

		private static void ConfigureApplication(bool askForLicence = false)
		{
			new ConfigurationController(Settings.Instance.CertificateFileName, askForLicence);
		}

		private static void InitializeApplication()
		{
			var licence = Settings.Instance.Licence;
			var certificatePassword = Settings.Instance.OTP;
			var context = new MVXContextWithCertificate(licence["applicationToken"], licence["applicationSecret"], Settings.Instance.CertificateFileName, certificatePassword);
			new MainController(context);
		}

		public static bool Validator(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
		{
			return true;
		}
	}
}
